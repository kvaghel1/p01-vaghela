//
//  AppDelegate.h
//  HelloWorld
//
//  Created by Kunal Vaghela on 1/23/17.
//  Copyright © 2017 Kunal Vaghela. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

