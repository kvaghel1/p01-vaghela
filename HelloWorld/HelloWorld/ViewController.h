//
//  ViewController.h
//  HelloWorld
//
//  Created by Kunal Vaghela on 1/23/17.
//  Copyright © 2017 Kunal Vaghela. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *changeButton;
@property (weak, nonatomic) IBOutlet UITextField *TextField;
@property (weak, nonatomic) IBOutlet UILabel *Label;

@end

