//
//  main.m
//  HelloWorld
//
//  Created by Kunal Vaghela on 1/23/17.
//  Copyright © 2017 Kunal Vaghela. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
